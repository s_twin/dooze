import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { GameModule } from './game/game.module';
import { RedisModule} from 'nestjs-redis'
require('dotenv').config()

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://" + process.env.mongo_url), 
    GameModule, 
    AuthModule,
    RedisModule.register({
      host: process.env.REDIS_HOST,
      port: Number(process.env.REDIS_PORT)
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
