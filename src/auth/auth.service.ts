import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class AuthService {

    constructor(
        @InjectModel(User.name) private UserModel: Model<UserDocument>
    ){}

    signUp = async (email, password) => {
        try{

            const newUser = await (new this.UserModel({email, password}).save())

            return {data: newUser};

        }catch(err){
            console.log(err);
            
            if(err.code == 11000){
                throw new HttpException('this user sign up before',400)
            }
            throw new HttpException('some thing went wrong',400)           
        }
    
        
    }

    signIn = async (email, password) => {

        const user = await this.UserModel.findOne({email,password}).exec();

        if(!user){
            return new HttpException('this username does not exist or your password is incorect',400)
        }
                
        const data ={
            id: user._id
        }

        return {data};

    }
}
