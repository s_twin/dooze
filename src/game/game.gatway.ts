import {
    ConnectedSocket,
    OnGatewayConnection,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer
} from '@nestjs/websockets';
import { Server } from 'socket.io';
import { RedisService } from 'nestjs-redis';

import { Socket } from 'socket.io'
import { GameService } from './game.service';

@WebSocketGateway(4000, { namespace: '/dooze' })
export class DoozeGame implements OnGatewayConnection {

    constructor( 
        private readonly redisService: RedisService, 
        private readonly gameService: GameService
        ){}
    private mapUserToSocketKey = 'map_user_id_socket_id'
    @WebSocketServer() wss: Server


    async handleConnection(client: Socket, info: any){

        console.log("<<< connect >>>");
        
        let clientId = client.id
        let userId = (client.handshake.query.id as string);

        if(!userId) client.disconnect();

        this.mapUserIdToSocketId(userId, clientId)
        
        const openGames = await this.gameService.openGame();

        setTimeout(() => {
            client.emit("init","wow")

            client.emit("open-game", openGames)
        }, 500)
        
        
        client.on('create-game', data => this.createGame(client, data))
        client.on('join-to-game', data => this.joinToGame(client,data))
        client.on('fill', data => this.fill(client, data))
        
    }

    private async createGame(client: Socket, data: any){
        const emitTo = 'create-game'

        const socketId = client.id
        const userId = await this.getUserIdFromSocketId(socketId)
        const { name } = data;
        
        const result = await this.gameService.createGame(userId, name)

        const gameId = result.data.id.toString()
        client.join(gameId)

        client.emit(`${emitTo}-owner`, result)
        this.wss.emit(emitTo, result)
        
    }

    private async joinToGame(client: Socket, data: any){
        const emitTo = 'join'

        const socketId = client.id
        const gameId = data["game_id"]
        const userId = await this.getUserIdFromSocketId(socketId)

        const response = await this.gameService.joinGame(gameId, userId)

        console.log(response);
        

        if( response.success ){
            client.join(gameId)

            client.emit(emitTo, response)
            client.broadcast.to(gameId).emit(emitTo, {success: true, data: "a person joined"})
        }else 
            client.emit(emitTo, response)

    }

    private async fill(client: Socket, data: any){

        const emitTo = 'fill'
        console.log("this ->>  ", data)
        const { row, col, game_id } = data;
        const userId = await this.getUserIdFromSocketId(client.id)        

        const result = await this.gameService.fillCell(row, col, game_id, userId);
        
        this.wss.to(game_id).emit(emitTo, result)
    }

    private mapUserIdToSocketId(userId: string, socketId: string){
        return new Promise((resolve, reject) => {
            this.redisService.getClient().hset(this.mapUserToSocketKey, [userId, socketId], (err, res) => {
                if(err) reject(err)
                else resolve(res)
            })
        })
        
    }

    private getSocketIdFromUserId(userId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.redisService.getClient().hget(this.mapUserToSocketKey, userId, (err, res) => {
                if(err) reject(err)
                else resolve(res)
            })
        })
    }

    private getUserIdFromSocketId(socketId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            this.redisService.getClient().hgetall(this.mapUserToSocketKey, (err, res) => {
                if(err) reject(err)
                else {
                    for(const [key, value] of Object.entries(res)){
                        if(value == socketId) return resolve(key)
                    }
                }
            })
        })
    }

}

