import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { DoozeGame } from './game.gatway'
import { MongooseModule } from '@nestjs/mongoose';
import { Dooze, DoozeSchema } from './schemas/dooze.schema'

@Module({
  imports : [
    MongooseModule.forFeature([{name: Dooze.name, schema: DoozeSchema}])
  ],
  providers: [GameService, DoozeGame]
})
export class GameModule {}
