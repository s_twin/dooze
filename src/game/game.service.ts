import { Injectable } from '@nestjs/common';
import { Dooze, DoozeDocument } from './schemas/dooze.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role } from './schemas/game.enum';

@Injectable()
export class GameService {

    constructor(
        @InjectModel(Dooze.name) private doozeModel: Model<DoozeDocument>
    ){}

    public async createGame(userId: string, name: string){
        try{           

            const game = new this.doozeModel({
                owner_id: userId,
                name: name
            })
            const data = await game.save()

            return {success: true , data: {id: data._id, name: game.name}};

        }catch(err){
            return {success: false};            
        }
    }


    /**
     * fillCell
     */
    public async fillCell(row, col, gameId, userId) {
        try {
            const game = await this.doozeModel.findOne({_id: gameId});

            // if(game.guest_id == null){
            //     return {success: false, data: {message: "you must wait to other one join to group"}}
            // }
            //TODO: prioritySend
            
            let role = Role.GUEST
            if(game.owner_id == userId) role = Role.OWNER;

            const allDetails = game.detail
            allDetails.push({role, row, col})

            const isWin = this.checkWin((allDetails as {role: Role, row: number, col: number}[]))
            console.log("Win",  isWin)
            if(isWin.success){
                return {success: true, data: { win: true, user_id: userId }}
            }
            
            const updateDooze = await this.doozeModel.findByIdAndUpdate(
                gameId,
                {$push: { 'detail': {row, col, role}}},
                {upsert: true, useFindAndModify: false}
            )            
            
            return {success: true, data: {win: false, row, col, role}}
            
        } catch (error) {
            console.log(error);
            return {success: false}
        }
        
    }

    private prioritySend(detail) {
        
    }

    public async joinGame(gameId: string, userId: string){
        try{

            const updatedDooze = await this.doozeModel.updateOne(
                {_id: gameId, guest_id: { $exists: true, $eq: null }}, 
                { $set: { guest_id: userId } }
            )

            if( updatedDooze.nModified )
                return { success: true, data: "you get the Game" }
            else
                return { success: false, data: "Game doesn't exist or sb start game" }

        }catch(error){
            return { success: false, data: error }
        }
    }

    public async openGame(){
        try{

            const openGames = await this.doozeModel.find({
                guest_id: { $exists: true, $eq: null }
            })

            return {success: true, data: openGames}
        }catch(err){
            return {success: false, data: null}
        }
    }

    public checkWin(cells: { role: Role, row: number, col: number }[]){

        const cellsRowSort = [...cells]
        const cellColumnSort = [...cells]

        cellsRowSort.sort((x, y) => {
            if(x.row > y.row) return 1
            if(x.row == y.row && x.col > y.col) return 1
            return -1
        })

        cellColumnSort.sort((x, y) => {
            if(x.col > y.col) return 1
            if(x.col == y.col && x.row > y.row) return 1
            return -1
        })

        for(let k in cellsRowSort){
            const n = Number(k)
            // console.log("--- Here ---", n, cellsRowSort)
            if(! (n + 2 <  cellsRowSort.length) ) break;

            if( 
                cellsRowSort[n].row == cellsRowSort[n + 1].row && 
                cellsRowSort[n + 1].row == cellsRowSort[n + 2].row &&

                cellsRowSort[n].role == cellsRowSort[n + 1].role &&
                cellsRowSort[n + 1].role == cellsRowSort[n + 2].role &&

                cellsRowSort[n].col == 1 &&
                cellsRowSort[n + 1].col == 2 && 
                cellsRowSort[n + 2].col == 3
            ) return { success: true, role: cellsRowSort[n].role }
            
        }

        for(let k in cellColumnSort){
            const n = Number(k)
            if(! (n + 2 <  cellColumnSort.length) ) break;

            if( 
                cellColumnSort[n].col == cellColumnSort[n + 1].col && 
                cellColumnSort[n + 1].col == cellColumnSort[n + 2].col &&

                cellColumnSort[n].role == cellColumnSort[n + 1].role &&
                cellColumnSort[n + 1].role == cellColumnSort[n + 2].role &&

                cellColumnSort[n].row == 1 &&
                cellColumnSort[n + 1].row == 2 && 
                cellColumnSort[n + 2].row == 3
            ) return { success: true, role: cellColumnSort[n].role }
            
        }
    
        const cellSame = cells.filter(cell => {
            return cell.row == cell.col
        })
        if(
            cellSame.length == 3 &&

            cellSame[0].role == cellSame[1].role &&
            cellSame[1].role == cellSame[2].role 

        ) return {success: true, role: cellSame[0].role}


        const cellMightSame = cells.filter(cell => {
            console.log("Detail -> ", cell.row, "        ", cell.col);
            if(cell.row == 2 && cell.col == 2) return true
            if(cell.row == 1 && cell.col == 3) return true
            if(cell.row == 3 && cell.col == 1) return true
            return false 
        })
        if(
            cellMightSame.length == 3 &&

            cellMightSame[0].role == cellMightSame[1].role &&
            cellMightSame[1].role == cellMightSame[2].role 

        ) return {success: true, role: cellMightSame[0].role}


        return {success: false}
    }

}
