import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { User } from '../../auth/schemas/user.schema';
import { Role } from './game.enum';

export type DoozeDocument = Dooze & Document

@Schema()
export class Dooze {

    @Prop({ type: String })
    name: string

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
    owner_id: mongoose.Types.ObjectId | User

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null })
    guest_id: mongoose.Types.ObjectId | User

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
    win: mongoose.Types.ObjectId | User

    @Prop({ type: Date })
    start_time: Date

    @Prop({ type: Date })
    end_time: Date

    @Prop({type: raw({
        role: { type: Role },
        row: { type: Number },
        column: { type: Number },
    }), default: []})
    detail: Record<string, any>[];
}

export const DoozeSchema = SchemaFactory.createForClass(Dooze);
