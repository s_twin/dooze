import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const PORT = 3000

  const app = await NestFactory.create(AppModule, {cors: true});
  await app.listen(PORT, () => {
    console.debug(`Server is running on port ${PORT} `)
  });

}
bootstrap();
